import axios from 'axios'


export default {
    createUser(userModel, authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/users`;
        return axios.post(url, userModel,
            {
                auth: {
                    username: authToken
                }
            }).then(response => {
                return response.data;
            })
    },
    getUserList(authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/users/`;
        return axios.get(url, {
            auth: {
                username: authToken
            }
        }).then((response) => {
            return response.data;
        })
    },
    getUserDetail(userId, authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/users/${userId}`;
        return axios.get(url, {
            auth: {
                username: authToken
            }
        }).then((response) => {
            return response.data;
        })
    },
    updateUsers(updateUser, authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/users/${updateUser.id}`;
        return axios.put(url, updateUser, {
            auth: {
                username: authToken
            }
        }).then(response => {
            return response.data;
        })
    },

}