import axios from 'axios'

export default {
    createSkill(skillModel, authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/library/skills`;
        return axios.post(url, skillModel,
            {
                auth: {
                    username: authToken
                }
            }).then(response => {
                return response.data;
            })
    },
    getSkills(authToken) {
        const url = `http://vuedev.aavaz.biz/aavazdev/library/skills`;
        return axios.get(url, {
            auth: {
                username: authToken
            }
        }).then((response) => {
            return response.data;
        })
    },
}