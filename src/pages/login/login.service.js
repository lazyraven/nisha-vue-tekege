import axios from 'axios'

const loginServices = {
    auth(authModel) {
        const url = `http://vuedev.aavaz.biz/aavazdev/users/auth`;
        return axios.post(url, {},
            { auth: authModel }
        ).then(response => {
            return response.data;
        });
    },
}

export default loginServices;