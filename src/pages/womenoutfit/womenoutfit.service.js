import axios from 'axios'

export default {
    getOutfits() {
        const url = `https://pim.wforwomanonline.com/pim/pimresponse.php`;

        return axios.get(url
        ).then((response) => {
            return response.data;
        })
    },
}