import Vue from 'vue'
import App from './App.vue'
import './index.css'
import VueRouter from 'vue-router'
import myRoutes from './router/index';
import VueCookies from 'vue-cookies';
import VueGoodTablePlugin from 'vue-good-table';
import Toasted from 'vue-toasted';
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(Toasted)
Vue.use(VueCookies);
Vue.use(VueGoodTablePlugin);

Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
  routes: myRoutes,
  mode: "history"
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
