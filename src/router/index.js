import HomePage from "../components/HomePage.vue";
import Login from "../pages/login/Login.vue";
import UserList from "../pages/user/UserList.vue";
import NewUser from "../pages/user/NewUser.vue";
import UserDetails from "../pages/user/UserDetails.vue";
import SkillList from "../pages/skills/SkillList.vue";
import NewSkill from "../pages/skills/NewSkill.vue";
import SideBar from "../pages/womenoutfit/SideBar.vue";
import Feature1 from "../pages/practice/Feature1.vue";


const routes = [
    { path: '/', name: 'dashboard', component: HomePage },
    { path: '/login', name: 'login', component: Login },
    { path: '/users', name: 'users', component: UserList },
    { path: '/users/new', name: 'newUser', component: NewUser },
    { path: '/users/:id', name: 'UserDetails', component: UserDetails },
    { path: '/skills', name: 'skills', component: SkillList },
    { path: '/skills/new', name: 'newSkills', component: NewSkill },
    { path: '/womenoutfit', name: 'sideBar', component: SideBar },
    { path: '/feature1', name: 'feature1', component: Feature1 },

]

export default routes;
